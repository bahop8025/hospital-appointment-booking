import { View, Text } from "react-native";
import React from "react";

export default function Line() {
  return (
    <View style={{ borderWidth: 1, borderColor: "#ccc", margin: 5 }}></View>
  );
}
