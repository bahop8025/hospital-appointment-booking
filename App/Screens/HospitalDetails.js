import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import React, { useEffect, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import PageHeader from "../Components/Shared/PageHeader";
import HospitalInfo from "../Components/HospitalDetail/HospitalInfo";

export default function HospitalDetails() {
  const [hospital, setHospital] = useState();
  // console.log(hospital, "hospital");
  const { params } = useRoute();
  const natigate = useNavigation();

  useEffect(() => {
    setHospital(params.hospital);
  }, []);

  return (
    hospital && (
      <View style={{ flex: 1 }}>
        <ScrollView>
          <View>
            <PageHeader title={" "} />
          </View>
          <View>
            <Image
              source={require("../../assets/chen1.png")}
              style={styles.image}
            />
            <View style={styles.hospitalInfo}>
              <HospitalInfo hospital={hospital} />
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity
          onPress={() =>
            natigate.navigate("book-appointment", { hospital: hospital })
          }
          style={styles.button}
        >
          <Text style={styles.text}>Book Appiontment</Text>
        </TouchableOpacity>
      </View>
    )
  );
}
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 300,
    objectFit: "contain",
  },
  hospitalInfo: {
    backgroundColor: "#ccc",
    marginTop: -20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
  },
  button: {
    padding: 13,
    backgroundColor: "red",
    margin: 10,
    borderRadius: 99,
    left: 0,
    right: 0,
    marginBottom: 20,
    zIndex: 10,
  },
  text: {
    color: "white",
    textAlign: "center",
  },
});
