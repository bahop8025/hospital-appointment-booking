import { View, Text, Button, StyleSheet, ScrollView } from "react-native";
import React from "react";
import { useAuth } from "@clerk/clerk-expo";
import Header from "../Components/home/Header";
import SearchBar from "../Components/home/SearchBar";
import Silder from "../Components/home/Silder";
import Category from "../Components/home/Category";
import PremiumHospitals from "../Components/home/PremiumHospitals";

export default function Home() {
  const { isLoaded, signOut } = useAuth();
  return (
    <ScrollView style={styles.container}>
      <Header />
      <SearchBar setSearchInput={(value) => console.log(value)} />
      <Silder />
      <Category />
      <PremiumHospitals />
      {/* <Button
        title="Sign Out"
        onPress={() => {
          signOut();
        }}
      /> */}
      {/* <Text>Home</Text> */}
    </ScrollView>
  );
}

const styles = StyleSheet.create({ container: { padding: 10, marginTop: 20 } });
