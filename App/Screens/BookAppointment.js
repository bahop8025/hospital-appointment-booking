import { useRoute } from "@react-navigation/native";
import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import BookAppointmentInfo from "../Components/BookAppointment/BookAppointmentInfo";
import BookingSection from "../Components/BookAppointment/BookingSection";
import Line from "./Line";

export default function BookAppointment() {
  const { params } = useRoute();
  console.log(params);

  return (
    <ScrollView style={styles.container}>
      <BookAppointmentInfo hospital={params.hospital} />
      <Line />
      <BookingSection hospital={params.hospital} />
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
});
