import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import app from "../../assets/adaptive-icon.png";
import SignInWithOAuth from "../Components/SignInWithOAuth";

export default function Login() {
  return (
    <View style={styles.container}>
      <Image source={app} style={styles.image} />
      <View style={styles.title}>
        <Text style={styles.heading}>Your Ultimate Doctor</Text>
        <Text>Neque porro quisquam est qui dolorem ipsum </Text>
        <SignInWithOAuth />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: { alignItems: "center", backgroundColor: "#ccc" },
  image: { width: 300, height: 500, objectFit: "contain" },
  title: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: "#fff",
    padding: 25,
    alignItems: "center",
  },
  heading: { fontSize: 26, fontWeight: "bold" },
});
