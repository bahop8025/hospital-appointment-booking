import React, { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import SubHeading from "../SubHeading";
import moment from "moment";
import { useUser } from "@clerk/clerk-expo";

export default function BookingSection({ hospital }) {
  const { isLoaded, isSignedIn, user } = useUser();
  // console.log(user.primaryEmailAddress.emailAddress, "user");

  const [next7Dates, setNext7Dates] = useState([]);
  const [timeList, setTimeList] = useState([]);
  const [seledate, setSelectDate] = useState("");
  const [seleTime, setSelectTime] = useState("");
  const [note, setNote] = useState("");

  useEffect(() => {
    getDays();
    getTimes();
  }, []);

  const getDays = () => {
    const today = moment();
    const nextSaveDays = [];
    for (let i = 1; i < 8; i++) {
      const date = moment().add(i, "days");
      nextSaveDays.push({
        date: date,
        day: date.format("dddd"),
        formartdate: date.format("MMM Do YY"),
      });
    }
    setNext7Dates(nextSaveDays);
  };

  const getTimes = () => {
    const timeList = [];
    for (let i = 8; i < 12; i++) {
      timeList.push({
        time: i + ":00 AM",
      });
      timeList.push({
        time: i + ":30 AM",
      });
    }
    for (let i = 1; i < 6; i++) {
      timeList.push({
        time: i + ":00 PM",
      });
      timeList.push({
        time: i + ":30 PM",
      });
    }
    setTimeList(timeList);
  };

  const bookApp = () => {
    const data = {
      data: {
        UserName: user.firstName,
        Date: seledate,
        Time: seleTime,
        Email: user.primaryEmailAddress.emailAddress,
        Hospitals: hospital.id,
        Note: note,
      },
    };
  };

  return (
    <View>
      <Text style={styles.title}>Book Appointment</Text>
      <SubHeading SubHeading="Day" seeAll={false} />
      <View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={next7Dates}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => setSelectDate(item.date)}
              style={[
                styles.date,
                seledate === item.date ? { backgroundColor: "red" } : null,
              ]}
            >
              <Text style={seledate === item.date ? { color: "#fff" } : null}>
                {item.day}
              </Text>
              <Text style={seledate === item.date ? { color: "#fff" } : null}>
                {item.formartdate}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
      <SubHeading SubHeading="Time" seeAll={false} />
      <View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={timeList}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => setSelectTime(item.time)}
              style={[
                styles.date,
                seleTime === item.time ? { backgroundColor: "red" } : null,
              ]}
            >
              <Text style={seleTime === item.time ? { color: "#fff" } : null}>
                {item.time}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
      <SubHeading SubHeading="Note" seeAll={false} />
      <View>
        <TextInput
          onChangeText={(value) => setNote(value)}
          placeholder="Enter...."
          // numberOfLines={4}
          multiline={true}
          style={styles.textInput}
        />
      </View>
      <TouchableOpacity onPress={() => bookApp()} style={styles.button}>
        <Text style={styles.text}>Make Appiontment</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  title: {
    color: "#ccc",
  },
  date: {
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    margin: 5,
    borderRadius: 30,
    alignItems: "center",
  },
  textInput: {
    height: 100,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
    padding: 5,
    textAlignVertical: "top",
  },
  button: {
    padding: 13,
    backgroundColor: "red",
    margin: 10,
    borderRadius: 99,
    left: 0,
    right: 0,
    marginBottom: 20,
    zIndex: 10,
  },
  text: {
    color: "white",
    textAlign: "center",
  },
});
