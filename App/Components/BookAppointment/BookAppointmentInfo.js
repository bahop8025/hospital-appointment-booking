import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import PageHeader from "../Shared/PageHeader";

export default function BookAppointmentInfo({ hospital }) {
  return (
    <View>
      <PageHeader headerTitle="Book Appointment" />
      <View style={styles.content}>
        <View style={{ width: "20%" }}>
          <Image
            style={styles.image}
            source={require("../../../assets/chen1.png")}
          />
        </View>
        <View style={styles.contentText}>
          <Text style={styles.id}>{hospital.id}</Text>
          <Text style={styles.text}>{hospital.title}</Text>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  content: {
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 10,
  },
  image: {
    height: 60,
    width: 60,
    objectFit: "contain",
  },
  contentText: {
    justifyContent: "center",
    width: "70%",
  },
  id: {
    fontWeight: "bold",
    fontSize: 21,
  },
  text: {
    fontSize: 14,
    textTransform: "capitalize",
  },
});
