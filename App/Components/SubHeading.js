import { View, Text, StyleSheet } from "react-native";
import React from "react";

export default function SubHeading({ SubHeading, seeAll = true }) {
  return (
    <View style={styles.wapper}>
      <Text style={styles.title}>{SubHeading}</Text>
      {seeAll ? (
        <Text style={[styles.title, styles.seeAll]}>See All</Text>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  wapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: 10,
    paddingTop: 10,
  },
  title: {
    fontWeight: "bold",
    fontFamily: "appFont-Light",
    fontSize: 18,
  },
  seeAll: {
    color: "blue",
    fontSize: 14,
  },
});
