import { View, Text, StyleSheet } from "react-native";
import React from "react";
import SubHeading from "../SubHeading";

export default function HospitalInfo({ hospital }) {
  return (
    hospital && (
      <View>
        <Text>{hospital.id}</Text>
        <Text>{hospital.title}</Text>
        <Text>{hospital.body}</Text>
        <SubHeading SubHeading={"About"} />
        <Text>{hospital.body}</Text>
      </View>
    )
  );
}

const styles = StyleSheet.create({});
