import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";
const img = require("../../../assets/chen1.png");

export default function HospitalItem({ item }) {
  return (
    <View style={styles.container}>
      <Image source={img} style={styles.image} />
      <Text style={styles.title}>{item.title}</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 140,
    objectFit: "contain",
  },
  title: {
    color: "#ccc",
    textTransform: "capitalize",
  },
  container: {
    marginTop: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    marginBottom: 10,
    padding: 10,
  },
});
