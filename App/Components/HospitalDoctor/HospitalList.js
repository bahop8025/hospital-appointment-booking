import React from "react";
import { FlatList, StyleSheet, TouchableOpacity } from "react-native";
import HospitalItem from "../Shared/HospitalItem";
import { useNavigation } from "@react-navigation/native";

export default function HospitalList({ listHospital }) {
  const naviagate = useNavigation();

  return (
    <FlatList
      data={listHospital}
      renderItem={({ item }) => (
        <TouchableOpacity
          onPress={() =>
            naviagate.navigate("Hospita-details", {
              hospital: item,
            })
          }
        >
          <HospitalItem item={item} />
        </TouchableOpacity>
      )}
    />
  );
}
const styles = StyleSheet.create({});
