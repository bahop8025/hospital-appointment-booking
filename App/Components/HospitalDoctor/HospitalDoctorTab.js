import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React, { useState } from "react";

export default function HospitalDoctorTab({ activeTab }) {
  const [activeIndex, setActiveIndex] = useState(0);
  return (
    <View style={styles.container}>
      <View style={styles.nav}>
        <TouchableOpacity
          onPress={() => {
            setActiveIndex(0);
            activeTab("Hospital");
          }}
        >
          <Text style={[activeIndex === 0 ? styles.active : styles.noActive]}>
            Hospita
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setActiveIndex(1);
            activeTab("Doctor");
          }}
        >
          <Text style={[activeIndex === 1 ? styles.active : styles.noActive]}>
            Doctor
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  nav: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignContent: "center",
  },
  active: {
    color: "blue",
    fontWeight: "bold",
  },
  noActive: {
    color: "black",
  },
});
