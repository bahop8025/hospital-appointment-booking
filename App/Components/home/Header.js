import { useUser } from "@clerk/clerk-expo";
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

export default function Header() {
  const { isLoaded, isSignedIn, user } = useUser();
  // console.log("---------------", user, "----------");

  if (!isLoaded || !isSignedIn) {
    return null;
  }

  return (
    <View style={styles.container}>
      <View style={styles.infomation}>
        <Image style={styles.image} source={{ uri: user.imageUrl }} />
        <View>
          <Text>Hello 👋,</Text>
          <Text style={styles.name}>{user.fullName}</Text>
        </View>
      </View>
      <View>
        <FontAwesome name="bell-o" size={24} color="black" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  infomation: {
    flexDirection: "row",
    gap: 10,
    alignItems: "center",
  },
  image: {
    height: 30,
    width: 30,
    borderRadius: 99,
  },
  name: {
    fontFamily: "appFont",
    color: "black",
    fontWeight: "bold",
    textTransform: "uppercase",
  },
});
