import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
} from "react-native";
import React, { useEffect, useState } from "react";
import GlobalAPI from "../../Services/GlobalAPI";
import SubHeading from "../SubHeading";
import { useNavigation } from "@react-navigation/native";

const imageCate = require("../../../assets/chen1.png");

export default function Category() {
  const navigate = useNavigation();
  const [category, setCategory] = useState([]);

  if (!category) {
    return null;
  }

  useEffect(() => {
    getCategories();
  }, []);

  const getCategories = () => {
    GlobalAPI.getCategories.then((response) => {
      setCategory(response.data);
    });
  };

  return (
    <View>
      <View style={styles.wapper}>
        <SubHeading SubHeading="Category" />
      </View>
      <FlatList
        data={category}
        renderItem={({ item, index }) =>
          index < 5 && (
            <TouchableOpacity
              onPress={() =>
                navigate.navigate("Docter", {
                  nameCate: item.title.slice(0, 5),
                })
              }
              style={styles.listCategory}
            >
              <View style={styles.itemImage}>
                <Image source={imageCate} style={styles.image} />
              </View>
              <Text>{item.title.slice(0, 5)}</Text>
            </TouchableOpacity>
          )
        }
        numColumns={5}
        columnWrapperStyle={{ flex: 1, justifyContent: "space-between" }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  image: {
    objectFit: "contain",
    width: 40,
    height: 40,
  },
  itemImage: { backgroundColor: "red", borderRadius: 99 },
  listCategory: { alignItems: "center" },
});
