import { View, Text, StyleSheet, FlatList } from "react-native";
import React, { useEffect, useState } from "react";
import SubHeading from "../SubHeading";
import HospitalsItem from "./HospitalsItem";
import GlobalAPI from "../../Services/GlobalAPI";

export default function PremiumHospitals() {
  const [premiumHospitals, setPremiumHospitals] = useState([]);

  if (!premiumHospitals) {
    return null;
  }

  useEffect(() => {
    getPremiumHospitals();
  }, []);

  const getPremiumHospitals = () => {
    GlobalAPI.getPremiumHospitals.then((response) => {
      setPremiumHospitals(response.data);
    });
  };

  return (
    <View>
      <SubHeading SubHeading="Our Premium Hospitals " />
      <FlatList
        horizontal={true}
        data={premiumHospitals.slice(0, 3)}
        renderItem={({ item, index }) => <HospitalsItem item={item} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
