import React from "react";
import { Dimensions, FlatList, Image, StyleSheet, View } from "react-native";

const dataSilider = [
  {
    id: 1,
    name: "Silider 1",
    img: require("../../../assets/slider1.jpg"),
  },
  {
    id: 2,
    name: "Silider 1",
    img: require("../../../assets/slider2.jpg"),
  },
];

export default function Silder() {
  return (
    <View style={styles.container}>
      <FlatList
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        data={dataSilider}
        renderItem={({ item, index }) => {
          return <Image source={item.img} style={styles.slider} />;
        }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  slider: {
    flex: 1,
    width: Dimensions.get("screen").width * 0.9,
    height: 160,
    borderRadius: 5,
    margin: 5,
  },
});
