import React, { useState } from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import Colors from "../../../assets/Colors";

export default function SearchBar({ setSearchInput }) {
  const [searchText, setSearchText] = useState("");
  return (
    <View>
      <View style={styles.search}>
        <FontAwesome5 name="search" size={20} color={Colors.GRAY} />
        <TextInput
          placeholder="Search"
          style={styles.input}
          onChangeText={(value) => setSearchText(value)}
          onSubmitEditing={() => setSearchInput(searchText)}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  search: {
    flexDirection: "row",
    alignItems: "center",
    padding: 8,
    height: 50,
    borderColor: Colors.GRAY,
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 10,
    marginBottom: 10,
  },
  input: { marginLeft: 10, width: "100%" },
});
