import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";
const image = require("../../../assets/slider1.jpg");

export default function HospitalsItem({ item }) {
  return (
    <View style={styles.hospitalItem}>
      <Image source={image} style={styles.image} />
      <View style={styles.content}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={[styles.title, styles.body]}>{item.body}</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 120,
    objectFit: "cover",
    borderRadius: 10,
  },
  content: { padding: 7 },
  hospitalItem: {
    width: 200,
    marginRight: 10,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
  },
  title: {
    textTransform: "capitalize",
    fontSize: 16,
    marginBottom: 5,
  },
  body: {
    textTransform: "capitalize",
    fontSize: 14,
    color: "#ccc",
  },
});
