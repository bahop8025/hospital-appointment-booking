import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import Docter from "../Screens/Docter";
import Home from "../Screens/Home";
import HospitalDetails from "../Screens/HospitalDetails";
import BookAppointment from "../Screens/BookAppointment";

const Stack = createStackNavigator();

export default function HomeNavigation() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Docter" component={Docter} />
      <Stack.Screen name="Hospita-details" component={HospitalDetails} />
      <Stack.Screen name="book-appointment" component={BookAppointment} />
    </Stack.Navigator>
  );
}
