import {
  AntDesign,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import Appointment from "../Screens/Appointment";
import Profile from "../Screens/Profile";
import HomeNavigation from "./HomeNavigation";

const Tab = createBottomTabNavigator();

export default function TabNavigation() {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen
        name="Home"
        component={HomeNavigation}
        options={{
          tabBarIcon: (color, size) => (
            <Ionicons name="home-outline" size={20} color="black" />
          ),
        }}
      />
      <Tab.Screen
        name="Appointment"
        component={Appointment}
        options={{
          tabBarIcon: (color, size) => (
            <AntDesign name="calendar" size={20} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: (color, size) => (
            <MaterialCommunityIcons
              name="account-outline"
              size={20}
              color="black"
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
