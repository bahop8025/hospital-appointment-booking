import axios from "axios";

const getCategories = axios.get("https://jsonplaceholder.typicode.com/todos");
const getPremiumHospitals = axios.get(
  "https://jsonplaceholder.typicode.com/posts"
);

export default {
  getCategories,
  getPremiumHospitals,
};
